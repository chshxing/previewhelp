package com.sx.rainlearn.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sx.rainlearn.R;
import com.sx.rainlearn.config.ConstanUrl;
import com.sx.rainlearn.utils.LogUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Administrator on 2018/11/1.
 */


public class VersionUpdatedialog extends Dialog {
    private TextView tvDownloadProgress;
    private TextView tvDownloadSpeed;
    private ProgressBar pbProgress;
    private Button btCancelUpdate;
//    private DownloadApk mDownloadApk;

    /**
     * 上一次的下载流大小
     */
    private long lastcurrent;
    /**
     * 上一次的下载时间
     */
    private long lastTime;

    private String updateUrl;
    private Context context;
    private Handler mHandler=new Handler();
    public VersionUpdatedialog(Context context, String updateurl) {
        super(context);
        context = context;
        updateUrl = updateurl;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_version_update);
        setWidthM_HeithW();
        this.setCanceledOnTouchOutside(false);
        initView();
        initListener();
        setInitContentShow();
//        downloadApk("");
        downloadApk(ConstanUrl.BASEURL+"upload/"+updateUrl);//http://120.77.42.11:8080/txxc.apk
    }

    /**
     * 宽度全屏，高度自动
     */
    public void setWidthM_HeithW() {
        // 设置窗口大小
        ViewGroup.LayoutParams params = getWindow().getAttributes();
        params.height = 600;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    /**
     * 初始化 View
     */
    private void initView() {
        tvDownloadProgress = (TextView) findViewById(R.id.tvDownloadProgress);
        tvDownloadSpeed = (TextView) findViewById(R.id.tvDownloadSpeed);
        pbProgress = (ProgressBar) findViewById(R.id.pbProgressBar);
        btCancelUpdate = (Button) findViewById(R.id.btCancelUpdate);
//        mDownloadApk		= new DownloadApk();
    }

    /**
     * 显示更新版本的内容
     */
    private void setInitContentShow() {
        pbProgress.setProgress(0);
        tvDownloadSpeed.setText("0kB/s");
        tvDownloadProgress.setText("0%");
    }

    /**
     * 初始化事件
     */
    private void initListener() {
        btCancelUpdate.setOnClickListener(clickListener);
    }

    /**
     * 控件点击事件处理
     */
    private android.view.View.OnClickListener clickListener = new android.view.View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                //取消更新
                case R.id.btCancelUpdate:

                    cancel();
                    break;

                default:
                    break;
            }
        }
    };

    /*
     * 版本正在升级
     */
    private void downloadApk(String path) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            String installPath = getContext().getExternalCacheDir() + "txxc.apk";
//            String installPath = Environment.getExternalStorageDirectory()
//                    .getAbsolutePath() + File.separator + "txxc.apk";
            final File file = new File(installPath);
            if (file.exists()) {
                file.delete();
            }
            download(path);

        } else {
            LogUtils.e("没有SD卡访问权限，无法下载apk文件");
            //没有SD卡访问权限
            Toast.makeText(getContext(), "请申请权限", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * @param url     下载连接
     */
    public void download(final String url) {
        //创建okHttpClient对象
        final OkHttpClient mOkHttpClient = new OkHttpClient();
        final Request request = new Request.Builder().url(url).build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {
                   @Override
                   public void onFailure(Call call, IOException e) {
                       cancel();

                   }
                  @Override
                  public void onResponse(Call call, Response response) throws IOException {

                      InputStream is = null;
                      byte[] buf = new byte[2048];
                      int len = 0;
                      FileOutputStream fos = null;
                      // 储存下载文件的目录
                      String savePath = isExistDir();
                      try {
                          is = response.body().byteStream();
                          long total = response.body().contentLength();
                          File file = new File(savePath, getNameFromUrl(url));
                          fos = new FileOutputStream(file);
                          long sum = 0;
                          while ((len = is.read(buf)) != -1) {
                              fos.write(buf, 0, len);
                              sum += len;
                              int progress = (int) (sum * 1.0f / total * 100);
                              // 下载中
                              final int index = len;
                              final int iddex1 = progress;
                              mHandler.post(new Runnable() {
                                  @Override
                                  public void run() {
                                      if (index > 0) {
                                          setSpeedShow((long) index, iddex1);
                                      }
                                  }
                              });

                          }
                          fos.flush();
                          // 下载完成
//                          Looper.prepare();
                          mHandler.post(new Runnable() {
                              @Override
                              public void run() {
//                                Toast.makeText(context,"下载完成",Toast.LENGTH_SHORT).show();
                                  btCancelUpdate.setText("下载完成");

                              }
                          });
//                          cancel();
//                        Looper.loop();
                      } catch (Exception e) {
                          e.printStackTrace();
                      } finally {
                          try {
                              if (is != null)
                                  is.close();
                          } catch (IOException e) {
                          }
                          try {
                              if (fos != null)
                                  fos.close();
                          } catch (IOException e) {
                          }
                      }
                  }
                                               }
        );


    }

    /**
     * @return
     * @throws IOException 判断下载目录是否存在
     */
    private String isExistDir() throws IOException {
        // 下载位置
        File downloadFile = new File(Environment.getExternalStorageDirectory(), "/"+"yxzs");
        if (!downloadFile.mkdirs()) {
            downloadFile.createNewFile();
        }
        String savePath = downloadFile.getAbsolutePath();
        return savePath;
    }

    /**
     * @param url
     * @return 从下载连接中解析出文件名
     */
    private String getNameFromUrl(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }


    /**
     * 设置当前下载速度显示
     *
     * @param current
     * @param index   下载进度
     */
    private void setSpeedShow(long current, int index) {
        tvDownloadSpeed.setText((current / 10) + "k/s");
        tvDownloadProgress.setText(index + " %");
        pbProgress.setProgress(index);   //设置下载进度
    }




    /**
     * 取得打开当前apk的运用方式
     *
     * @param var0
     * @return
     */
    private String getMIMEType(File var0) {
        String var1 = "";
        String var2 = var0.getName();
        String var3 = var2.substring(var2.lastIndexOf(".") + 1, var2.length()).toLowerCase();
        var1 = MimeTypeMap.getSingleton().getMimeTypeFromExtension(var3);
        return var1;
    }
}