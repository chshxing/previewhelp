package com.sx.rainlearn.ui;

import android.app.Application;
import android.content.Context;

/**
 * Created by williambai on 2018/10/14.
 */

public class MyApp extends Application {
    public static Context myContext;
    @Override
    public void onCreate() {
        super.onCreate();
        myContext=this;
    }

}
