package com.sx.rainlearn.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sx.rainlearn.mvp.present.BasePresent;

/**
 * Created by Administrator on 2018/10/20.
 */

public abstract class BaseFragment<T extends BasePresent> extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewOrListen();
        initPresenter();
        loadData();
    }

    protected abstract void initViewOrListen();
    public abstract void initPresenter();
    protected abstract void loadData();



}
