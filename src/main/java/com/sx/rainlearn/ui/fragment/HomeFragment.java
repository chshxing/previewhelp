package com.sx.rainlearn.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sx.rainlearn.R;
import com.sx.rainlearn.mvp.module.ResponseEntity;
import com.sx.rainlearn.mvp.present.HomeFragmentPresent;
import com.sx.rainlearn.mvp.view.HomeFragmentView;
import com.sx.rainlearn.ui.MyApp;
import com.sx.rainlearn.ui.activity.CurseActivity;
import com.sx.rainlearn.ui.activity.UpLoadFileActivity;
import com.sx.rainlearn.utils.UiUtils;
import com.sx.rainlearn.widget.ButtomBtn;
import com.sx.rainlearn.widget.FlyBanner;
import com.sx.rainlearn.widget.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/20.
 */

public class HomeFragment extends BaseFragment<HomeFragmentPresent> implements HomeFragmentView {
    @BindView(R.id.bannerTop)
    FlyBanner vbannerTop;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.bbtn_cursor)
    ButtomBtn bbtn_cursor;
    @BindView(R.id.bbtn_document)
    ButtomBtn bbtn_document;
    @BindView(R.id.bbtn_collect)
    ButtomBtn bbtn_collect;
    @BindView(R.id.pager_tabstrip)
    PagerSlidingTabStrip pager_tabstrip;
    @BindView(R.id.vp) ViewPager vp;
    @BindView(R.id.iv_addcurser)
    ImageView iv_addcurser;
    private List<String> bigPics;
    private String[] datas;
    private HomeFragmentPresent homeFragmentPresent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        int screenWidth = UiUtils.getScreenWidth(MyApp.myContext);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(screenWidth/2, 120);
        pager_tabstrip.setLayoutParams(layoutParams);
        return view;

    }

    @Override
    protected void initViewOrListen() {
        tv_title.setText(R.string.app_name);
        iv_addcurser.setOnClickListener(clickListen);
        datas = new String[]{"我教的课", "我听得课"};
        bbtn_cursor.setIvAndTv(R.mipmap.course,UiUtils.getString(R.string.str_home_curse));
        bbtn_cursor.setOnClickListener(clickListen);
        bbtn_document.setIvAndTv(R.mipmap.document,UiUtils.getString(R.string.str_home_docu));
        bbtn_collect.setIvAndTv(R.mipmap.collect,UiUtils.getString(R.string.str_home_collect));


        pager_tabstrip.setViewPager(vp);
        for (int i = 0; i < datas.length; i++) {
            View v = UiUtils.getInflate().inflate(R.layout.fragment_base_viewpage_tab_item,null);
            TextView titl = (TextView) v.findViewById(R.id.tab_title);
            titl.setText(datas[i]);
            pager_tabstrip.addTab(v);
        }
    }

    private View.OnClickListener clickListen=new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.iv_addcurser:     //   新增课件
                    startActivity(new Intent(getActivity(), UpLoadFileActivity.class));
                    break;
                case R.id.bbtn_cursor:      //   查看课件
                    startActivity(new Intent(getActivity(), CurseActivity.class));
                    break;
            }
        }
    };

    @Override
    public void initPresenter() {
        homeFragmentPresent = new HomeFragmentPresent(MyApp.myContext, this);
        homeFragmentPresent.init();
    }

    @Override
    protected void loadData() {
        initBannerPics();
        initBannerTop();
        initFragment();
        homeFragmentPresent.register("填入参数");
    }


    private void initBannerTop() {
        vbannerTop.setImagesUrl(bigPics);
        vbannerTop.setOnItemClickListener(new FlyBanner.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
//                UiUtils.showToast("position--->"+position);
            }
        });
    }

    private void initFragment() {
        List<MyCurseFragment> listFragment=new ArrayList<>();

        for (String data : datas) {
            MyCurseFragment myCurseFragment = new MyCurseFragment();
            listFragment.add(myCurseFragment);
        }
        MyAdapter myAdapter = new MyAdapter(getActivity(), listFragment,getFragmentManager());
        vp.setAdapter(myAdapter);
    }

    private void initBannerPics() {
        bigPics = new ArrayList<>();
        bigPics.add("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=839483303,2300556296&fm=11&gp=0.jpg");
        bigPics.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1540400902685&di=8ff20054a2b8569b96e736c489a9f91a&imgtype=0&src=http%3A%2F%2Fwww.zjjyb.cn%2Fres%2F1%2F1%2F2017-05%2F12%2F04%2Fres07_attpic_brief.jpg");
        bigPics.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1540400948582&di=0dd7fdb7e18b8fe049ff31f27ee46f2d&imgtype=0&src=http%3A%2F%2Fpic.qqtn.com%2Fup%2F2016-6%2F2016060116270566630.png");
        bigPics.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1540401022017&di=b56c6947cd7faa844aec21010b93752b&imgtype=0&src=http%3A%2F%2Fs8.sinaimg.cn%2Fmw690%2F005XIuMAzy785FYSCuH37%26690");
        bigPics.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1540401048310&di=621a784d11045dbaff84615e4f33454b&imgtype=0&src=http%3A%2F%2Fimg.mp.itc.cn%2Fupload%2F20160429%2F54714b5f75ab4457acf877678dc51d28_th.jpg");
    }

    @Override
    public void init() {

    }

    @Override
    public void showProgress() {
        //  TODO 显示加载进度条
    }

    @Override
    public void hideProgress() {
        //  TODO 隐藏加载进度条
    }

    @Override
    public void showErrorView(String str) {
        //  TODO 显示错误信息
    }

    @Override
    public void showData(ResponseEntity responseEntity) {
        //  TODO 网络请求返回数据  刷新view
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        homeFragmentPresent.release();
    }

    public static class MyAdapter extends FragmentPagerAdapter {

        private List<MyCurseFragment> listFragment;
        private Context context;

        private MyAdapter(Context context, List<MyCurseFragment> listFragment, FragmentManager manager) {
            super(manager);
            this.listFragment = listFragment;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            if (position == 0) {
                bundle.putString("content", "点击+号创建课程");
            } else {
                bundle.putString("content", "我听的课");
            }

            MyCurseFragment myCurseFragment = listFragment.get(position);
            myCurseFragment.setArguments(bundle);
            return listFragment.get(position);
        }


        @Override
        public int getCount() {
            return listFragment.size();
        }


    }



}
