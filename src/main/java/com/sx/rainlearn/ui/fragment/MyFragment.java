package com.sx.rainlearn.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sx.rainlearn.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/21.
 * 我的
 */

public class MyFragment extends BaseFragment {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @Override
    protected void initViewOrListen() {
            tv_title.setText("我的");
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void loadData() {

    }
}
