package com.sx.rainlearn.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sx.rainlearn.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/21.
 */

public class MessageChildFragment extends BaseFragment {
    @BindView(R.id.tv_messagechild)
    TextView tv_messagechild;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messagechild, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    protected void initViewOrListen() {
        Bundle content = getArguments();
        if(content!=null){
            String str= (String) content.get("content");
            tv_messagechild.setText(str);
        }
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void loadData() {

    }
}
