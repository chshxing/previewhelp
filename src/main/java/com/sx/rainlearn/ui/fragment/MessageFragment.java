package com.sx.rainlearn.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sx.rainlearn.R;
import com.sx.rainlearn.utils.UiUtils;
import com.sx.rainlearn.widget.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/21.
 * 消息
 */

public class MessageFragment extends BaseFragment {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.vip)
    ViewPager vip;
    @BindView(R.id.pager_tabstrip)
    PagerSlidingTabStrip pager_tabstrip;
    @BindView(R.id.rel_include_title)
    RelativeLayout rel_include_title;
    private String[] datas;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_msg, container, false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    protected void initViewOrListen() {
        tv_title.setText("消息");
        datas = new String[]{"通知", "评论","私信"};
        pager_tabstrip.setViewPager(vip);
        for (int i = 0; i < datas.length; i++) {
            View v = UiUtils.getInflate().inflate(R.layout.fragment_base_viewpage_tab_item,null);
            TextView titl = (TextView) v.findViewById(R.id.tab_title);
            titl.setText(datas[i]);
            pager_tabstrip.addTab(v);
        }
    }

    @Override
    public void initPresenter() {

    }

    @Override
    protected void loadData() {
        initFragment();
    }
    private void initFragment() {
        List<MessageChildFragment> listFragment=new ArrayList<>();

        for (String data : datas) {
            MessageChildFragment mesChildFragment = new MessageChildFragment();
            listFragment.add(mesChildFragment);
        }
        MyAdapter myAdapter = new MyAdapter(getActivity(), listFragment,getFragmentManager());
        vip.setAdapter(myAdapter);
    }

    public static class MyAdapter extends FragmentPagerAdapter {

        private List<MessageChildFragment> listFragment;
        private Context context;

        private MyAdapter(Context context, List<MessageChildFragment> listFragment, FragmentManager manager) {
            super(manager);
            this.listFragment = listFragment;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            if (position == 0) {
                bundle.putString("content", "查看通知");
            } else if(position==1) {
                bundle.putString("content", "还没有收到评论");
            }else if(position==2) {
                bundle.putString("content", "查看私信");
            }

            MessageChildFragment msgChild = listFragment.get(position);
            msgChild.setArguments(bundle);
            return listFragment.get(position);
        }


        @Override
        public int getCount() {
            return listFragment.size();
        }


    }


}
