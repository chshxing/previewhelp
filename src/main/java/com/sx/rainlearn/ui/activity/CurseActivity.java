package com.sx.rainlearn.ui.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.sx.rainlearn.CurseAdapter;
import com.sx.rainlearn.R;
import com.sx.rainlearn.config.ConstanUrl;
import com.sx.rainlearn.mvp.module.FileEntity;
import com.sx.rainlearn.mvp.present.CurseActivityPresent;
import com.sx.rainlearn.mvp.view.CurseActivityView;
import com.sx.rainlearn.utils.UiUtils;
import com.tencent.connect.share.QQShare;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/10/31.
 * 查看所有课件
 */

public class CurseActivity extends BaseActivity implements CurseActivityView {
    @BindView(R.id.rec_allcurse)
    RecyclerView rec_allcurse;
    @BindView(R.id.tv_title)
    TextView tv_title;

    private Tencent tencent;
    private CurseActivityPresent curseActivityPresent;

    @Override
    protected int setLayouId() {
        return R.layout.activity_allcurse;
    }

    @Override
    protected void initViewOrListen() {
        tv_title.setText("我的课件");
        tencent = Tencent.createInstance("1107946850", CurseActivity.this);

    }

    public void share(String fileName,String fileDesc) {
        Bundle bundle = new Bundle();
        //这条分享消息被好友点击后的跳转URL。
        bundle.putString(QQShare.SHARE_TO_QQ_TARGET_URL, ConstanUrl.BASEURL+ConstanUrl.FILEPATH+fileName);
        //分享的标题。注：PARAM_TITLE、PARAM_IMAGE_URL、PARAM_SUMMARY不能全为空，最少必须有一个是有值的。
        bundle.putString(QQShare.SHARE_TO_QQ_TITLE, fileName);
        //分享的图片URL
        bundle.putString(QQShare.SHARE_TO_QQ_TARGET_URL, ConstanUrl.BASEURL+ConstanUrl.FILEPATH+fileName);
        //分享的消息摘要，最长50个字
        bundle.putString(QQShare.SHARE_TO_QQ_SUMMARY, fileDesc);
        //手Q客户端顶部，替换“返回”按钮文字，如果为空，用返回代替
        bundle.putString(QQShare.SHARE_TO_QQ_APP_NAME, fileName);
        //标识该消息的来源应用，值为应用名称+AppId。
//        bundle.putString(QQShare.PARAM_APP_SOURCE, "星期几" + AppId);

        tencent.shareToQQ(this, bundle ,new BaseUiListener());

    }
    private class BaseUiListener implements IUiListener {
        @Override
        public void onComplete(Object o) {

        }

        @Override

        public void onError(UiError e) {


        }

        @Override

        public void onCancel() {


        }

    }

    @Override
    protected void initPresent() {
        curseActivityPresent = new CurseActivityPresent(this, this);
        curseActivityPresent.loadAllCuse();
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showErrorView(String str) {

    }

    @Override
    public void init() {
        requestPermission();
    }
    private void requestPermission(){
        boolean permission = UiUtils.isPermission(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
        if(!permission){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                requestPermissions(new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                },10);

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < grantResults.length; i++) {
            boolean isTip = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]);
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                if (isTip) {//表明用户没有彻底禁止弹出权限请求
                    requestPermission();
                } else {//表明用户已经彻底禁止弹出权限请求
                    //   PermissionMonitorService.start(this);//这里一般会提示用户进入权限设置界面
                    UiUtils.showToast("文件未授权");
                    finish();
                }
                return;
            }
        }
    }

    private List<FileEntity> list;
    @Override
    public void showCurse(List<FileEntity> list) {
        this.list=list;
        CurseAdapter adapter = new CurseAdapter(this,list);
        rec_allcurse.setLayoutManager(new LinearLayoutManager(this));
        adapter.setShareClickListen(clickListen);
        rec_allcurse.setAdapter(adapter);

    }
    private CurseAdapter.ShareClickListen clickListen=new CurseAdapter.ShareClickListen() {
        @Override
        public void onClickListen(int position) {

            if(list!=null){
                FileEntity fileEntity = list.get(position);
                if(fileEntity!=null){
                    String fileName = fileEntity.getFileName();
                    String filedescription = fileEntity.getFiledescription();
                    share(fileName,filedescription);
                }

            }
        }
    };
}
