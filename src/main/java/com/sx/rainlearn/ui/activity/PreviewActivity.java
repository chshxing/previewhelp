package com.sx.rainlearn.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.sx.rainlearn.R;
import com.sx.rainlearn.ui.MyApp;
import com.sx.rainlearn.utils.LogUtils;
import com.sx.rainlearn.utils.UiUtils;
import com.sx.rainlearn.widget.ZoomableImageView;

import java.io.File;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/11/3.
 */

public class PreviewActivity extends BaseActivity {
    @BindView(R.id.zoomImageview)
    ZoomableImageView zoomImageview;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private static final String FILENAME="fileName";
    private static Intent intent;

    public static Intent setImageUrl(Context context,String imgUrl){
        intent = new Intent(context,PreviewActivity.class);
        intent.putExtra(FILENAME,imgUrl);
       return intent;
    }

    @Override
    protected int setLayouId() {
        return R.layout.activity_preview;
    }

    @Override
    protected void initViewOrListen() {

    }

    @Override
    protected void initPresent() {
        loadData();
    }

    @Override
    protected void loadData() {
        if(intent!=null){
            String stringExtra = intent.getStringExtra(FILENAME);
            displayPi(stringExtra,R.mipmap.ic_launcher_web);
        }
    }

    /**
     * 加载图片，路劲为网络或者本地
     * @param fileName
     * @param resId
     */
    private void displayPi(String fileName,int resId){
        if(fileName.startsWith("http")){
            Picasso.with(MyApp.myContext)
                    .load(fileName)
                    .error(resId)
                    .config(Bitmap.Config.RGB_565)
                    .into(new MyViewHolder(zoomImageview));
        }else {
            File file = new File(fileName);
            Picasso.with(MyApp.myContext)
                    .load(file)
                    .error(resId)
                    .config(Bitmap.Config.RGB_565)

                    .into(new MyViewHolder(zoomImageview));


        }

    }

    private  class MyViewHolder implements Target{
        private ZoomableImageView zoom;
        public MyViewHolder(ZoomableImageView zoom){
            this.zoom=zoom;
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            progressBar.setVisibility(View.GONE);
            LogUtils.d("Picasso--onBitmapLoaded");
            zoom.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            LogUtils.d("Picasso-- onBitmapFailed");
            UiUtils.showToast("加载失败，请先下载");
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            LogUtils.d("Picasso--onPrepareLoad");
            progressBar.setVisibility(View.VISIBLE);
//            Bitmap bitmap = BitmapFactory.decodeResource(MyApp.myContext.getResources(), R.mipmap.ic_launcher_web);
//            zoom.setImageBitmap(bitmap);

        }
    }

}
