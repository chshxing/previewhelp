package com.sx.rainlearn.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sx.rainlearn.R;
import com.sx.rainlearn.help.ApiProgressListener;
import com.sx.rainlearn.help.FileApiModel;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/10/31.
 */

public class UpLoadFileActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.et_upcurxe_desc)EditText et_upcurxe_desc;
    @BindView(R.id.et_upcurxe_auth)EditText et_upcurxe_auth;
    @BindView(R.id.btn_upcurse)Button btn_upcurse;
    @BindView(R.id.iv_addfile)ImageView iv_addfile;
    @BindView(R.id.tv_upcurse_path)TextView tv_upcurse_path;
    @BindView(R.id.progressBar)ProgressBar progressBar;


    @Override
    protected int setLayouId() {
        return R.layout.activity_upcurse;
    }

    @Override
    protected void initViewOrListen() {
        tv_title.setText("点击图标选择文件");

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            requestPermissions(new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            },10);

        }
        btn_upcurse.setOnClickListener(clickListen);
        iv_addfile.setOnClickListener(clickListen);
    }


    private View.OnClickListener clickListen=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                  switch (v.getId()){
                      case R.id.iv_addfile:        //   选择文件
                          Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                          intent.setType("*/*");//无类型限制
                          intent.addCategory(Intent.CATEGORY_OPENABLE);
                          startActivityForResult(intent, 1);
                          break;

                      case R.id.btn_upcurse:        //   上传课件
                          String desc = et_upcurxe_desc.getText().toString();
                          if(TextUtils.isEmpty(desc)){
                              desc="未知文件描述";
                          }
                          String auth = et_upcurxe_auth.getText().toString();
                          if(TextUtils.isEmpty(auth)){
                              auth="未知作者";
                          }
                          upCurse(auth,desc);
                          break;

                  }
        }
    };

    //简单上传有请求数据
    public void upCurse(String auth,String filedescription) {
        Map<String, String> map = new HashMap<>();
        map.put("author", auth);
        map.put("filedescription", filedescription);
        String s = tv_upcurse_path.getText().toString();
        if(!s.contains("文件路径")){
            File file = new File(s);
            FileApiModel.uploadPhoto2(map, file, getProgressListener()).subscribe(new Consumer<String>() {
                @Override
                public void accept(String s) throws Exception {
                        Toast.makeText(UpLoadFileActivity.this,"上传成功",Toast.LENGTH_SHORT).show();
                }
            });
        }else {
            Toast.makeText(this,"请选择文件",Toast.LENGTH_SHORT).show();
        }
    }
    public ApiProgressListener getProgressListener() {
        return new ApiProgressListener() {
            @Override
            public void onProgress(long currentSize, long totalSize, boolean done) {
                int progress = (int) (100D * currentSize / totalSize);
                Log.e("上传", "onProgress--" + progress + "  " + done);
                progressBar.setMax(100);
                progressBar.setProgress(progress);
            }
        };
    }
    public Subscriber<String> getSubscriber() {
        return new Subscriber<String>(){
            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(String s) {

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        };
    }
    String path;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if ("file".equalsIgnoreCase(uri.getScheme())){//使用第三方应用打开
                path = uri.getPath();
                tv_upcurse_path.setText(path);
                Toast.makeText(this,path+"11111",Toast.LENGTH_SHORT).show();
                return;
            }
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {//4.4以后
                path = getPath(this, uri);
                tv_upcurse_path.setText(path);
                Toast.makeText(this,path,Toast.LENGTH_SHORT).show();
            } else {//4.4以下下系统调用方法
                path = getRealPathFromURI(uri);
                tv_upcurse_path.setText(path);
//                Toast.makeText(UpLoadFileActivity.this, path+"222222", Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * 专为Android4.4设计的从Uri获取文件绝对路径，以前的方法已不好使
     */
    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }
    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * 获取数据
     */
    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public String getRealPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if(null!=cursor&&cursor.moveToFirst()){;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
            cursor.close();
        }
        return res;
    }


    @Override
    protected void initPresent() {

    }

    @Override
    protected void loadData() {

    }
}
