package com.sx.rainlearn.ui.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.sx.rainlearn.mvp.present.BasePresent;
import com.sx.rainlearn.utils.UiUtils;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/10/20.
 */

public abstract class BaseActivity<T extends BasePresent> extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayouId());
        ButterKnife.bind(this,this);
        ActionBar supportActionBar = getSupportActionBar();
        if(supportActionBar!=null){supportActionBar.hide();}
        UiUtils.setStatus(this);
        initViewOrListen();
        initPresent();

    }



    protected abstract int setLayouId();

    protected abstract void initViewOrListen();

    protected abstract void initPresent();
    protected abstract void loadData();
    private ProgressDialog pb;
    public void showProgress(){
        if(pb==null){
            pb=new ProgressDialog(this);
            pb.show();
        }
    }
    public void dimissProgress(){
        if(pb!=null){
            if(pb.isShowing()){
                pb.dismiss();
            }
        }
    }

}
