package com.sx.rainlearn.ui.activity;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.sx.rainlearn.R;
import com.sx.rainlearn.ui.fragment.HomeFragment;
import com.sx.rainlearn.ui.fragment.MessageFragment;
import com.sx.rainlearn.ui.fragment.MyFragment;

import butterknife.BindView;

/**
 * Created by Administrator on 2018/10/20.
 * 主页
 */

public class MainActity extends BaseActivity {
    @BindView(R.id.frame_main)
    FrameLayout frame_main;
    @BindView(R.id.navigation)
    BottomNavigationView mnavigation;
    private Fragment mfragment;
    private FragmentManager fragmentManager;

    @Override
    protected int setLayouId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViewOrListen() {
        fragmentManager = getSupportFragmentManager();
        mnavigation.setOnNavigationItemSelectedListener(bottomListen);
        mnavigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    protected void initPresent() {

    }

    private BottomNavigationView.OnNavigationItemSelectedListener bottomListen= new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId()){
                case R.id.navigation_home:
                    if(mfragment!=null){
                        transaction.hide(mfragment);
                    }
                    Fragment homeFragment =  fragmentManager.findFragmentByTag(HomeFragment.class.getName());
                    if(homeFragment!=null){
                        transaction.show(homeFragment);
                    }else {
                        homeFragment = new HomeFragment();
                        transaction.add(R.id.frame_main,homeFragment,HomeFragment.class.getName());
                    }


                             mfragment=homeFragment;
                    break;
                case R.id.navigation_dashboard:

                    if(mfragment!=null){
                        transaction.hide(mfragment);
                    }
                    Fragment messageFragment =  fragmentManager.findFragmentByTag(MessageFragment.class.getName());
                    if(messageFragment!=null){
                        transaction.show(messageFragment);
                    }else {
                            messageFragment = new MessageFragment();
                            transaction.add(R.id.frame_main,messageFragment,MessageFragment.class.getName());
                    }

                    mfragment=messageFragment;
                    break;
                case R.id.navigation_notifications:
                    Fragment myFragment =  fragmentManager.findFragmentByTag(MyFragment.class.getName());
                    if(mfragment!=null){
                        transaction.hide(mfragment);
                    }
                    if(myFragment!=null){
                        transaction.show(myFragment);
                    } else {
                            myFragment = new MyFragment();
                            transaction.add(R.id.frame_main,myFragment,MyFragment.class.getName());

                    }

                    mfragment=myFragment;
                    break;
            }
            transaction.commit();
            return true;
        }
    };

    @Override
    protected void loadData() {

    }
}
