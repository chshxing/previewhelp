package com.sx.rainlearn.utils;

import android.util.Log;



public class LogUtils {
	private final static boolean Debug = true;
	private static final String MODULE = "TAG";
	
	public static void d(String msg){
		if(!Debug){
			return;
		}
		final Throwable throwable = new Throwable();
		final StackTraceElement [] elements = throwable.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String [] classStrings = callerClassName.split("\\.");
		final String callerMethodName = elements[1].getMethodName();
		final int callerLinNum = elements[1].getLineNumber();
		Log.d(MODULE+"->"+classStrings[classStrings.length-1] ,  "["+callerMethodName+"->"+callerLinNum+"]->"+msg);
	}
	public static void d(){
		LogUtils.d("");
	}
	public static void d(int i) {
		LogUtils.d(""+i);
	}
	public static void d(Boolean b) {
		LogUtils.d(""+b);
	}
	public static void e(String msg){
		if(!Debug){
			return;
		}
		final Throwable throwable = new Throwable();
		final StackTraceElement [] elements = throwable.getStackTrace();
		final String callerClassName = elements[1].getClassName();
		final String [] classStrings = callerClassName.split("\\.");
		final String callerMethodName = elements[1].getMethodName();
		final int callerLinNum = elements[1].getLineNumber();
		Log.e(MODULE+"->"+classStrings[classStrings.length-1] ,  "["+callerMethodName+"->"+callerLinNum+"]->"+msg);
	}
}
