package com.sx.rainlearn.http;


import com.sx.rainlearn.config.ConstanUrl;
import com.sx.rainlearn.mvp.module.FileEntity;
import com.sx.rainlearn.mvp.module.ResponseEntity;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by williambai on 2018/10/14.
 */

public interface MessageRetrofit {
    /**注册*/
    @GET(ConstanUrl.register)
    Observable<ResponseBody> register();
    /**获得余额*/
    @GET(ConstanUrl.REMAINMONEY)
    Observable<ResponseEntity> getRemain(@Query("shopId") String shopId);
    /**微信充值二维码*/
    @GET(ConstanUrl.WXCODE)
    Observable<ResponseEntity> getWXcode(@Query("shopId") String shopId, @Query("money") String money, @Query("telPhone") String telPhone);

    @GET(ConstanUrl.ALLCURSE)
    Observable<List<FileEntity>>  findAllCurse(@Query("userName") String userName);
    @FormUrlEncoded
    @POST(ConstanUrl.SENDMESSAGE)
    Observable<ResponseEntity> sendmessage(@Field("shopId") String shopId, @Field("telphone") String telphone, @Field("messageContent") String messageContent);
}
