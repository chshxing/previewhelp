package com.sx.rainlearn.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.sx.rainlearn.config.ConstanUrl;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by williambai on 2018/10/14.
 */

public class RetrofitClient {
    private static MessageRetrofit messageRetrofit;
    protected static final Object monitor = new Object();
    private static Retrofit retrofit;

    private RetrofitClient(){

    }

    static{
        Gson date_gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        retrofit = new Retrofit.Builder()
                .baseUrl(ConstanUrl.BASEURL)
//                .baseUrl("填写网络主机地址")
//                .baseUrl(ConstanUrl.BASEURLTEST)
                .addConverterFactory(GsonConverterFactory.create(date_gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public static MessageRetrofit getGankRetrofitInstance() {
        synchronized (monitor) {
            if (messageRetrofit == null) {
                messageRetrofit = retrofit.create(MessageRetrofit.class);
            }
            return messageRetrofit;
        }
    }
}
