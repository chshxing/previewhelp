package com.sx.rainlearn.mvp.view;

import com.sx.rainlearn.mvp.module.FileEntity;

import java.util.List;

/**
 * Created by Administrator on 2018/10/31.
 */

public interface CurseActivityView extends IbaseView{

    void showCurse(List<FileEntity> list);
}
