package com.sx.rainlearn.mvp.view;

/**
 * Created by Administrator on 2018/10/15.
 */

public interface IbaseView {
    /**加载进度*/
    void showProgress();
    /**隐藏进度*/
    void hideProgress();
    /**网络错误 etc*/
    void showErrorView(String str);
    void init();
}
