package com.sx.rainlearn.mvp.view;

import com.sx.rainlearn.mvp.module.ResponseEntity;

/**
 * Created by Administrator on 2018/10/24.
 */

public interface HomeFragmentView extends IbaseView {

    /**显示数据*/
    void showData(ResponseEntity responseEntity);

}
