package com.sx.rainlearn.mvp.present;

import android.content.Context;

import com.sx.rainlearn.http.RetrofitClient;
import com.sx.rainlearn.mvp.module.FileEntity;
import com.sx.rainlearn.mvp.view.CurseActivityView;
import com.sx.rainlearn.utils.LogUtils;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Administrator on 2018/10/31.
 */

public class CurseActivityPresent extends BasePresent<CurseActivityView> {
    public CurseActivityPresent(Context context, CurseActivityView ibaseView) {
        super(context, ibaseView);
    }

    @Override
    public void release() {
        if (disposable != null&&!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public void loadAllCuse(){
        ibaseView.showProgress();
        disposable= RetrofitClient.getGankRetrofitInstance().findAllCurse("123")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<FileEntity>>() {
                    @Override
                    public void accept(@NonNull List<FileEntity> reponse) throws Exception {
                        ibaseView.hideProgress();
                        if(reponse!=null&&reponse.size()>0){
                            LogUtils.d("请求结果"+reponse.toString());
                            ibaseView.showCurse(reponse);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        ibaseView.showErrorView("网络错误");
                        ibaseView.hideProgress();
                    }
                });
    }
}
