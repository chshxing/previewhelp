package com.sx.rainlearn.mvp.present;

import android.content.Context;

import com.sx.rainlearn.http.RetrofitClient;
import com.sx.rainlearn.mvp.view.HomeFragmentView;
import com.sx.rainlearn.utils.LogUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * Created by Administrator on 2018/10/24.
 */

public class HomeFragmentPresent extends BasePresent<HomeFragmentView> {
    public HomeFragmentPresent(Context context, HomeFragmentView ibaseView) {
        super(context, ibaseView);
    }

    @Override
    public void release() {
        if (disposable != null&&!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public void register(String shopId){
        ibaseView.showProgress();
        disposable= RetrofitClient.getGankRetrofitInstance().register()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(@NonNull ResponseBody reponse) throws Exception {
                        ibaseView.hideProgress();
                        String string = reponse.string();
                        LogUtils.d("请求结果"+string);
//                        Toast.makeText(MyApp.myContext,"请求结果"+ string,Toast.LENGTH_LONG).show();
//                        if (responseEntity.result == null) {
//                            ibaseView.showErrorView("账号不存在");
//                        } else {
//                            ibaseView.showData(responseEntity);
//                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        ibaseView.showErrorView("网络错误");
                        ibaseView.hideProgress();
                    }
                });
    }



}
