package com.sx.rainlearn.mvp.present;

import android.content.Context;

import com.sx.rainlearn.mvp.view.IbaseView;

import io.reactivex.disposables.Disposable;

/**
 * Created by Administrator on 2018/10/15.
 */

public abstract class BasePresent<T extends IbaseView> {
    protected Disposable disposable;
     protected T ibaseView;
     protected Context context;
     public BasePresent(Context context,T ibaseView){
         this.context=context;
         this.ibaseView=ibaseView;
     }

     public void init(){
         ibaseView.init();
     }


     public abstract void release();



}
