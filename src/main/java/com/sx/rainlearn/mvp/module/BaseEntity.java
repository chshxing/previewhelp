package com.sx.rainlearn.mvp.module;

import java.lang.reflect.Field;

/**
 * Created by Administrator on 2018/10/15.
 */

public class BaseEntity {
    int result;

    public String toString(){
        Class<? extends BaseEntity> clazz = this.getClass();
        Field[] fields = clazz.getDeclaredFields();
        StringBuilder sbu = new StringBuilder();
            sbu.append("{");
        for(int i=0;i<fields.length;i++){
            Field field=fields[i];
            field.setAccessible(true);
            if(i!=0){
                sbu.append(",");
            }
            String name = field.getName();
            sbu.append(name);
            sbu.append("=");
            try {
                Object o = field.get(this);
                sbu.append(o);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sbu.append("}");
        return sbu.toString();

    }
}
