package com.sx.rainlearn.mvp.module;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 文件实体
 * @author Administrator
 *
 */

public class FileEntity extends BaseEntity{
	private int id;
	private String author;
	private String userName;
	private String fileName;
	private double fileSize;
	private String filepath;
	private String filedescription;
	private String updatetime;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public double getFileSize() {
		return fileSize;
	}
	public void setFileSize(double fileSize) {
		this.fileSize = fileSize;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getFiledescription() {
		return filedescription;
	}
	public void setFiledescription(String filedescription) {
		this.filedescription = filedescription;
	}

	public String getUpdatetime() {
		return converTime(updatetime);
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	public String converTime(String str) {
		SimpleDateFormat sdb = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		Date parse = null;
		try {
			parse = sdb.parse(str);
			sdb=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return  sdb.format(parse);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";


	}

}
