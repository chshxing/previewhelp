package com.sx.rainlearn.mvp.module;


/**
 * Created by williambai on 2018/10/14.
 */

public class ResponseEntity extends BaseEntity {
    public String result;
    public String data;
    public int status;
    public ResponseEntity() {
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public void setData(String data) {
        this.data = data;
    }
}
