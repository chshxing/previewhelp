package com.sx.rainlearn;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sx.rainlearn.config.ConstanUrl;
import com.sx.rainlearn.config.WpsModel;
import com.sx.rainlearn.mvp.module.FileEntity;
import com.sx.rainlearn.ui.activity.PreviewActivity;
import com.sx.rainlearn.ui.dialog.VersionUpdatedialog;
import com.sx.rainlearn.utils.LogUtils;
import com.sx.rainlearn.utils.UiUtils;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 2018/11/1.
 */

public class CurseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<FileEntity> list;
    private Context context;
    public CurseAdapter(Context context, List<FileEntity> list){
        this.list=list;
        this.context=context;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = UiUtils.getInflate().inflate(R.layout.item_curse_activity, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).bindData(list.get(position));
        ((ViewHolder) holder).iv_share_curse.setTag(position);

    }

    ShareClickListen shareClickListen;
    public void setShareClickListen(ShareClickListen shareClickListen){
        this.shareClickListen=shareClickListen;
    }


    public interface ShareClickListen{
        void onClickListen(int position);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.iv_file_curse) ImageView iv_file_curse;
        @BindView(R.id.tv_name_curse) TextView tv_name_curse;
        @BindView(R.id.tv_curse_desc) TextView tv_curse_desc;
        @BindView(R.id.tv_curse_date) TextView tv_curse_date;
        @BindView(R.id.tv_curse_author) TextView tv_curse_author;
        @BindView(R.id.iv_down_curse) ImageView iv_down_curse;
        @BindView(R.id.iv_share_curse) ImageView iv_share_curse;
        FileEntity fileEntity;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            iv_down_curse.setOnClickListener(clickListen);
            iv_share_curse.setOnClickListener(clickListen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String fileName = fileEntity.getFileName();
                    String filePath = fileIsExis(fileName);
                    if(isPic(fileName)){
                        if(!TextUtils.isEmpty(filePath)){      //  在 zoomImage缩放图片
                            context.startActivity(PreviewActivity.setImageUrl(context,filePath));

                        }else {
                            context.startActivity(PreviewActivity.setImageUrl(context,ConstanUrl.BASEURL+ConstanUrl.FILEPATH+fileName));
                        }
                    }else if(isDoc(fileName)){         //  开启打开方式
                        if(!TextUtils.isEmpty(filePath)){
                            boolean b = openFile(filePath);
                            LogUtils.d("文件打开"+b);
                        }else{
                            UiUtils.showToast("请下载课件");
                        }

                    }
//                    VersionUpdatedialog dialog = new VersionUpdatedialog(context, );
//                     dialog.show();
                }
            });
        }

        private View.OnClickListener clickListen=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.iv_down_curse:
                        VersionUpdatedialog dialog = new VersionUpdatedialog(context, fileEntity.getFileName());
                        dialog.show();
                        break;
                    case R.id.iv_share_curse:
                        int position= (int) iv_share_curse.getTag();
                        if(shareClickListen!=null){
                            shareClickListen.onClickListen(position);
                        }
                        break;
                }
            }
        };

        public void bindData(FileEntity fileEntity){
            this.fileEntity=fileEntity;
            String fileName = fileEntity.getFileName();
            String author = fileEntity.getAuthor();
            if(TextUtils.isEmpty(author)){
                tv_curse_author.setVisibility(View.GONE);
            }else {
                tv_curse_author.setVisibility(View.VISIBLE);
                tv_curse_author.setText("作者："+author);
            }
            String filedescription = fileEntity.getFiledescription();
            String updatetime = fileEntity.getUpdatetime();
            if(fileName.endsWith(".jpg")||fileName.endsWith(".jpeg")||fileName.endsWith(".bmp")){
                displayPi(fileName,R.mipmap.defaultjpg);
            }else if(fileName.endsWith(".doc")||fileName.endsWith(".docx")){
                displayPi(fileName,R.mipmap.txt);
            }else if(fileName.endsWith(".txt")){
                displayPi(fileName,R.mipmap.txt);
            }else {
                iv_file_curse.setImageResource(R.mipmap.txt);
            }
            if(!TextUtils.isEmpty(fileName)){
                String[] split = fileName.split("\\.");
                tv_name_curse.setText(split[0]);
            }
            tv_curse_desc.setText(filedescription);
            tv_curse_date.setText(updatetime);
        }
        private void displayPi(String fileName,int resId){
            Picasso.with(context)
                    .load(ConstanUrl.BASEURL+ConstanUrl.FILEPATH+fileName)
            .error(resId)
                    .config(Bitmap.Config.RGB_565)
                    .into(iv_file_curse);
        }
    }

    /**
     * 判断文件是否存在
     * @param fileName
     */
    private String fileIsExis(String fileName) {
        String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+"yxzs";
        File file = new File(absolutePath, fileName);
        if(file.exists()){
            return file.getAbsolutePath();
        }
        return null;

    }

    /**
     * 判断是否为图片
     * @param fileName
     */
    private boolean isPic(String fileName) {
        if(!TextUtils.isEmpty(fileName)){
            if(fileName.endsWith(".jpg")||fileName.endsWith(".jpeg")||fileName.endsWith(".png")){
                return true;
            }
        }
        return false;
    }
       /**
     * 判断是否为doc文件
     * @param fileName
     */
    private boolean isDoc(String fileName) {
        if(!TextUtils.isEmpty(fileName)){
            if(fileName.endsWith(".txt")||fileName.endsWith(".doc")||fileName.endsWith(".docx")){
                return true;
            }
        }
        return false;
    }

    /**
     * 打开.txt或者.doc文件
     * @param path
     * @return
     */
   private boolean openFile(String path) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(WpsModel.OPEN_MODE, WpsModel.OpenMode.NORMAL); // 打开模式
        bundle.putBoolean(WpsModel.SEND_CLOSE_BROAD, true); // 关闭时是否发送广播
        bundle.putString(WpsModel.THIRD_PACKAGE, context.getPackageName()); // 第三方应用的包名，用于对改应用合法性的验证
        bundle.putBoolean(WpsModel.CLEAR_TRACE, true);// 清除打开记录
        // bundle.putBoolean(CLEAR_FILE, true); //关闭后删除打开文件
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setClassName(WpsModel.PackageName.NORMAL, WpsModel.ClassName.NORMAL);

        File file = new File(path);
        if (file == null || !file.exists()) {
            System.out.println("文件为空或者不存在");
            return false;
        }

        Uri uri = Uri.fromFile(file);
        intent.setData(uri);
        intent.putExtras(bundle);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            System.out.println("打开wps异常："+e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
