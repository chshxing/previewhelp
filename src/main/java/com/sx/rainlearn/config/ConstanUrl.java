package com.sx.rainlearn.config;

/**
 * Created by Administrator on 2018/10/24.
 * 网络资源
 */

public interface ConstanUrl {
//    String BASEURL="http://192.168.43.19:8080/Rain/";
    String BASEURL="https://go.eir2.com/Rain/";
    String FILEPATH="upload/";
    String BASEURLTEST="测试主机名";
    /**授权*/
    String register="https://www.baidu.com/";
    /**查询余额*/
    String REMAINMONEY="资源地址";
    /**充值二维码*/
    String WXCODE="资源地址";
    /**发送短信*/
    String SENDMESSAGE="资源地址";
    /**查看所有课件*/
    String ALLCURSE="findAllAction";
}
